
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/* Version INFO 15.11.2021   
Author: LDae
Change: new Module/File due to Redesign

*/



typedef unsigned short int UINT16;
typedef unsigned char UINT8;

void automat(UINT8, UINT8*);

void get_input(char*, UINT8*);
void put_output(char*, UINT8);

int main ()
{	
	UINT8 x = 0;
	UINT8 y = 0;
	int count = 0;
	do
	{								
		get_input ("Code Coverage TC1", &x);
		automat( x, &y ); 
		put_output("Simulation", y);
		/* sleep(1); */ /* non-busy waiting */
		/* Pause for other tasks */
	
		getchar();
		count++;
	} 
	while( count < 10);	
	return 0;
}

